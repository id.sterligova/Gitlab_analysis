import pandas as pd

def test_births_dataframe():
    births = pd.read_csv('data/births.csv')
    assert not births.empty  # Проверяем, что DataFrame births не пустой

def test_monthly_births():
    births = pd.read_csv('data/births.csv')
    births['date'] = pd.to_datetime(births['date'])
    births['month'] = births['date'].dt.month
    monthly_births = births.groupby('month')['births'].sum()
    assert len(monthly_births) == 12  # Проверяем, что у нас есть данные для каждого месяца
