#!/usr/bin/env python
# coding: utf-8

# # Exercise: Analysis - Births
# 
# <img src = "https://static01.nyt.com/images/2020/01/28/well/physed-babies/physed-babies-mobileMasterAt3x.jpg" width = 300>

import pandas as pd

births = pd.read_csv('data/births.csv')
births.head()

# ## Task: Calculate the number of births for each calendar month

# ## First approach

# We need to have the date in the datetime format.
# Here they are well formatted, so there are no obstacles.
births['date'] = pd.to_datetime(births['date'])
births.head()

# Quick viz
births.plot(x="date", y="births")

# Let's extract the year and the month
births['year'] = births['date'].dt.year
births['month_as_a_nb'] = births['date'].dt.month
births['week'] = births['date'].dt.isocalendar().week  # Changed from .dt.week to .dt.isocalendar().week
births['dow'] = births['date'].dt.dayofweek
births['month'] = births['date'].dt.month_name() # we could also use "month" instead of "month_name()", see the difference in the documentation
births.head()

# Simple group by
births.groupby(['year', 'month'])['births'].sum().plot()

# ## Second approach

# Group only by month
res = births.groupby('month')['births'].sum()
res.plot(kind="bar")
# Создаем график из DataFrame
ax = res.plot(kind="bar")

# Получаем объект Figure
fig = ax.get_figure()

# Сохраняем график в PDF
fig.savefig('plot.pdf')

# It is sorted in alphabetical order...
# Let's use month nb instead
births['month'] = births['date'].dt.month
res2 = births.groupby('month')['births'].sum()
res2.plot(kind="bar")

ax2 = res2.plot(kind="bar")

# Получаем объект Figure
fig2 = ax2.get_figure()

# Сохраняем график в PDF
fig2.savefig('plot2.pdf')
