# Используем базовый образ Python 3.9
FROM python

# Устанавливаем рабочую директорию внутри контейнера
WORKDIR /app

# Копируем файлы requirements.txt и ваш Python-скрипт в контейнер
COPY Analysis_Births.py .

# Устанавливаем зависимости из requirements.txt
RUN pip install pandas

# Команда, которая будет выполнена при запуске контейнера
CMD ["python", "Analysis_Births.py"]



